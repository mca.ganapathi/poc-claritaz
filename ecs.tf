data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "fargate" {
  source = "../../"

  name_prefix = "ecs-fargate-example"
  # sg_name_prefix     = "my-security-group-name" # uncomment if you want to name security group with specific name

  vpc_id             = data.aws_vpc.default.id
  private_subnet_ids = data.aws_subnet_ids.all.ids
  cluster_id         = aws_ecs_cluster.cluster.id
  target_groups = [
    {
      target_group_name = "external-alb"
      container_port    = 80
    },
    {
      target_group_name = "internal-alb"
      container_port    = 80
    }
  ]

  wait_for_steady_state = true

  platform_version = "1.4.0" # defaults to LATEST

  task_container_image   = "marcincuber/2048-game:latest"
  task_definition_cpu    = 256
  task_definition_memory = 512

  task_container_port             = 80
  task_container_assign_public_ip = true

  health_check = {
    port = "traffic-port"
    path = "/"
  }

  task_stop_timeout = 90

  depends_on = [
    module.external-alb,
    module.internal-alb
  ]

  ### To use task credentials, below paramaters are required
  # create_repository_credentials_iam_policy = false
  # repository_credentials                   = aws_secretsmanager_secret.task_credentials.arn
}

resource "aws_security_group" "allow_sg_test" {
  name        = "allow_sg_test"
  description = "Allow sg inbound traffic"
  vpc_id      = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "test_sg_ingress" {
  security_group_id        = aws_security_group.allow_sg_test.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 3022
  to_port                  = 3022
  source_security_group_id = module.fargate.service_sg_id
}

